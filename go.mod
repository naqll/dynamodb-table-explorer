module gitlab.com/naqll/dynamodb-table-explorer

go 1.18

require (
	github.com/aws/aws-sdk-go v1.44.61
	github.com/gdamore/tcell/v2 v2.5.1
	github.com/rivo/tview v0.0.0-20220709181631-73bf2902b59a
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20220318055525-2edf467146b5 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
