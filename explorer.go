package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"gitlab.com/naqll/dynamodb-table-explorer/pkg/tables"
	"gitlab.com/naqll/dynamodb-table-explorer/ui/pages"
)

type Explorer struct {
	*tview.Application

	Pages         *tview.Pages
	TablePage     *pages.TablePage     // The default page with DynamoDB table list and item explorer
	TableInfoPage *pages.TableInfoPage // Popup modal containing DynamoDB table details
	QueryPage     *pages.QueryPage     // Page for running queries
	JSONPage      *pages.JSONPage      // Page for displaying copyable DynamoDB items in JSON format
	ErrorPage     *pages.ErrorPage     // Page for displaying errors to the user

	DescribedTables map[string]*tables.Table // All DynamoDB tables that have had DescribeTable called on them
}

// NewExplorer returns an Explorer root tview application
func NewExplorer(disableTableTrim, disableJSONColors, disableJSONBorder bool, rowCount int) *Explorer {
	ex := &Explorer{
		Application: tview.NewApplication(),
		Pages:       tview.NewPages(),

		TablePage:     pages.NewTablePage(disableTableTrim, rowCount),
		TableInfoPage: pages.NewTableInfoPage(),
		QueryPage:     pages.NewQueryPage(disableTableTrim),
		JSONPage:      pages.NewJSONPage(disableJSONColors, disableJSONBorder),
		ErrorPage:     pages.NewErrorPage(),

		DescribedTables: make(map[string]*tables.Table),
	}

	// Initialize sub-resources
	ex.TablePage.PopulateTableList(dynamoTableNames)

	// Pages, and their sub-resources, will have their own unique input captures but may require knowledge
	// of other sub-resources therefore we'll set all non-trivial inputs here in Explorer
	ex.setTablePageInputCaptures()
	ex.setQueryPageInputCaptures()
	ex.setTableInfoPageInputCaptures()
	ex.setJSONPageInputCaptures()
	ex.setErrorPageInputCapture()

	// Add pages
	ex.Pages.AddPage(ex.TablePage.GetTitle(), ex.TablePage, true, true)
	ex.Pages.AddPage(ex.TableInfoPage.GetTitle(), ex.TableInfoPage, true, false)
	ex.Pages.AddPage(ex.QueryPage.GetTitle(), ex.QueryPage, true, false)
	ex.Pages.AddPage(ex.JSONPage.GetTitle(), ex.JSONPage, true, false)
	ex.Pages.AddPage(ex.ErrorPage.GetTitle(), ex.ErrorPage, true, false)

	// Set global inputs
	ex.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		// Anything handled here will be executed on the main thread
		switch event.Key() {
		case tcell.KeyRune:
			// We'll catch letter/rune keys here to map J, K, H and L to the arrow keys unless we're in a form in
			// which case we'll delegate control to the form
			if ex.QueryPage.Form.HasFocus() {
				return event
			}

			switch event.Rune() {
			case 'j':
				return tcell.NewEventKey(tcell.KeyDown, 'j', tcell.ModNone)
			case 'k':
				return tcell.NewEventKey(tcell.KeyUp, 'k', tcell.ModNone)
			case 'h':
				return tcell.NewEventKey(tcell.KeyLeft, 'h', tcell.ModNone)
			case 'l':
				return tcell.NewEventKey(tcell.KeyRight, 'l', tcell.ModNone)
			}
		case tcell.KeyCtrlC:
			ex.Stop()
			return nil
		}
		return event
	})

	return ex
}

// SetCurrentTableSelection sets the TablePage.CurrentTableSelection and returns it
func (ex *Explorer) SetCurrentTableSelection() (*tables.Table, error) {
	table, err := ex.getTableDetails(ex.TablePage.CurrentTableSelectionName())
	if err != nil {
		return nil, err
	}
	ex.TablePage.CurrentTableSelection = table
	return table, nil
}

// DisplayJSON pretty prints the data, writes it to JSONPage, and then shows JSONPage.
// No coloring is applied to the data, however tcell dynamic color values are still respected
// dependent on the -disable-json-colors flag.
func (ex *Explorer) DisplayJSON(data interface{}) error {
	if err := ex.JSONPage.PrettyPrintJSON(data); err != nil {
		return err
	}
	ex.Pages.ShowPage(ex.JSONPage.GetTitle())
	return nil
}

// DisplayJSONItem colors the Item keys/values if coloring is enabled, pretty prints it, writes it to JSONPage,
// and then shows JSONPage
func (ex *Explorer) DisplayJSONItem(item tables.Item) error {
	if ex.JSONPage.ColorsEnabled {
		coloredItem := item.ColoredCopy()
		if err := ex.JSONPage.PrettyPrintJSON(coloredItem); err != nil {
			return err
		}
	} else {
		if err := ex.JSONPage.PrettyPrintJSON(item); err != nil {
			return err
		}
	}

	ex.Pages.ShowPage(ex.JSONPage.GetTitle())
	return nil
}

// DisplayError writes the error message on the ErrorPage and then shows the ErrorPage
func (ex *Explorer) DisplayError(err error) {
	ex.ErrorPage.WriteError(err)
	ex.Pages.ShowPage(ex.ErrorPage.GetTitle())
}

// showTableInfoPage sets the current table selection, populates the table metadata/info, and shows the TableInfoPage
func (ex *Explorer) showTableInfoPage() {
	if dynamoTableNames == nil {
		return
	}
	if tableDetails, err := ex.SetCurrentTableSelection(); err != nil {
		ex.DisplayError(fmt.Errorf("error getting table details: %s", err.Error()))
	} else {
		ex.TableInfoPage.PopulateTableInfo(tableDetails)
		ex.Pages.ShowPage(ex.TableInfoPage.GetTitle())
	}
}

// switchToQueryPage sets the current table selection, populates the query form, and switches to QueryPage
func (ex *Explorer) switchToQueryPage() {
	if dynamoTableNames == nil {
		return
	}

	table, err := ex.SetCurrentTableSelection()
	if err != nil {
		ex.DisplayError(fmt.Errorf("error getting table details: %s", err.Error()))
		return
	}

	ex.QueryPage.PopulateForm(table)
	ex.QueryPage.PopulateColumnHeader(table.SortedKeyNames)
	ex.Pages.SwitchToPage(ex.QueryPage.GetTitle())
}

// getTableDetails returns the TableDetails for the given table name. If the table has been previously described
// it is returned from the DescribedTables map.
// Otherwise, it is described and stored in the map before being returned.
func (ex *Explorer) getTableDetails(tableName string) (*tables.Table, error) {
	if table, ok := ex.DescribedTables[tableName]; ok {
		return table, nil
	}

	tableDesc, err := describeTable(tableName)
	if err != nil {
		return nil, fmt.Errorf("error describing DynamoDB table: %s", err.Error())
	}

	newTable := tables.NewTableDetails(tableDesc, dynamoClient)
	ex.DescribedTables[tableName] = newTable
	return newTable, nil
}

// describeTable returns the described DynamoDB table
func describeTable(tableName string) (*dynamodb.TableDescription, error) {
	input := &dynamodb.DescribeTableInput{
		TableName: &tableName,
	}

	resp, err := dynamoClient.DescribeTable(input)
	if err != nil {
		return nil, err
	}

	return resp.Table, nil
}
