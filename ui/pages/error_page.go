package pages

import (
	"github.com/rivo/tview"
)

type ErrorPage struct {
	*tview.Grid
	ErrorInfo *tview.TextView
}

func (_ ErrorPage) GetTitle() string {
	return "ErrorPage"
}

// NewErrorPage returns an ErrorPage used to display error messages in a popup window
func NewErrorPage() *ErrorPage {
	errorText := tview.NewTextView().SetDynamicColors(true)
	errorText.SetTitle("ERROR")
	errorText.SetBorder(true)
	errorText.SetTextAlign(tview.AlignCenter)

	grid := tview.NewGrid().
		SetColumns(0, 100, 0).
		SetRows(0, 20, 0).
		AddItem(errorText, 1, 1, 1, 1, 0, 0, true)

	return &ErrorPage{
		Grid:      grid,
		ErrorInfo: errorText,
	}
}

// WriteError writes the error text to the TextView
func (e *ErrorPage) WriteError(err error) {
	e.ErrorInfo.SetText(err.Error())
}
