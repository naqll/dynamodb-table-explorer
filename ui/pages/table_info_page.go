package pages

import (
	"fmt"

	"github.com/rivo/tview"

	"gitlab.com/naqll/dynamodb-table-explorer/pkg/tables"
)

type TableInfoPage struct {
	*tview.Grid
	Info *tview.TextView
}

// NewTableInfoPage returns a new Grid used for displaying DynamoDB table metadata
func NewTableInfoPage() *TableInfoPage {
	infoText := tview.NewTextView().SetDynamicColors(true)
	infoText.SetBorder(true)
	infoText.SetTextAlign(tview.AlignCenter)

	grid := tview.NewGrid().
		SetColumns(0, 100, 0).
		SetRows(0, 20, 0).
		AddItem(infoText, 1, 1, 1, 1, 0, 0, true)

	return &TableInfoPage{
		Grid: grid,
		Info: infoText,
	}
}

func (_ TableInfoPage) GetTitle() string {
	return "TableInfo"
}

// PopulateTableInfo populates the TextView with DynamoDB table metadata
func (t *TableInfoPage) PopulateTableInfo(tableDetails *tables.Table) {
	t.Info.SetTitle(*tableDetails.TableName)
	t.Info.SetText(formatTableDetails(tableDetails))
}

// TODO this is gross and needs to be reworked
// formatTableDetails formats the metadata of a table with colored output for display in a TextView
func formatTableDetails(tableDetails *tables.Table) string {
	// Optional range key
	var rangeKeyText string
	if tableDetails.RangeKey != nil {
		rangeKeyText = fmt.Sprintf("[green]RangeKey: [white]%s\n ", *tableDetails.RangeKey)
	}

	// Table size -- AWS uses 1000 bytes per KB/MB/GB, not 1024
	var tableSizeText string
	if *tableDetails.TableSizeBytes > 1000000000 {
		tableSizeText = fmt.Sprintf("%.2f GB", float64(*tableDetails.TableSizeBytes)/1000/1000/1000)
	} else if *tableDetails.TableSizeBytes > 1000000 {
		tableSizeText = fmt.Sprintf("%.2f MB", float64(*tableDetails.TableSizeBytes)/1000/1000)
	} else if *tableDetails.TableSizeBytes > 1000 {
		tableSizeText = fmt.Sprintf("%.2f KB", float64(*tableDetails.TableSizeBytes)/1000)
	} else {
		tableSizeText = fmt.Sprintf("%d B", *tableDetails.TableSizeBytes)
	}

	// Gather global secondary indexes
	var globalSecondaryText string
	if tableDetails.GlobalSecondaryIndexes != nil {
		globalSecondaryText = " Global Secondary Indexes:[pink]"
		for i, gsi := range tableDetails.GlobalSecondaryIndexes {
			globalSecondaryText += fmt.Sprintf("\n          %d. %s", i, *gsi.IndexName)
		}
		globalSecondaryText += "[white]"
	}

	// Table status
	var status string
	if *tableDetails.TableStatus == "ACTIVE" {
		status = "[green]ACTIVE[white]"
	} else {
		status = fmt.Sprintf("[red]%s[white]", *tableDetails.TableStatus)
	}

	// Format it all in a super gross way
	text := fmt.Sprintf(`

    [green]Status: %s
[green]Hash Key: [white]%s
 %s [yellow]Item Count: [white]%d 
       [yellow]Size: [white]%s

  %s


  [blue]ARN: [white]%s
`, status, *tableDetails.HashKey, rangeKeyText, *tableDetails.ItemCount, tableSizeText, globalSecondaryText, *tableDetails.TableArn)

	return text
}
