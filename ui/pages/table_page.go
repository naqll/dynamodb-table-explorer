package pages

import (
	"fmt"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"gitlab.com/naqll/dynamodb-table-explorer/pkg/tables"
)

type TablePage struct {
	*tview.Grid

	DynamoTableList *tview.List
	ScannedItems    *tview.Table
	HelpFooter      *tview.TextView

	CurrentTableSelection *tables.Table // The currently-selected table

	// Very large table items cause tcell rendering to lag.
	// This is exacerbated when large queries are run.
	// If trimTableRows is true, table item attributes will be trimmed to 60 characters.
	// The full item is still viewable in the JSON view.
	trimTableRows  bool
	trimTableWidth int

	PreviewRowCount int // The number of rows to load when previewing a table
}

const (
	tableListHelpText = `   [green]Enter: [white]Scan preview of table                         [green]D/F2: [white]Show table details     [green]K/↑: [white]Move up
       [green]Tab: [white]Switch between table list and table items     [green]Q/F3: [white]Query table            [green]J/↓: [white]Move down`
	scannedItemsHelpText = `    [green]Enter: [white]Display item JSON                                      [green]K/↑: [white]Move up     [green]H/←: [white]Scroll left
       [green]Tab: [white]Switch between table list and table items              [green]J/↓: [white]Move down   [green]L/→: [white]Scroll right`
)

const defaultTableTrimWidth = 60

// NewTablePage returns a new table page holding the left column of DynamoDB tables and the right view of scanned items
func NewTablePage(disableTableTrim bool, rowCount int) *TablePage {
	// Setting dynamic colors allows parsing the '[color]' colors in the constants
	footer := tview.NewTextView().SetDynamicColors(true).SetTextAlign(tview.AlignCenter)

	tableList := tview.NewList()
	tableList.ShowSecondaryText(false).
		SetBorder(true).
		SetTitle("Tables")

	scannedItemsTable := tview.NewTable()
	scannedItemsTable.SetSelectable(true, false).
		SetBorders(true).
		SetFixed(1, 1).
		SetTitle("Scanned Items")

	// Display the respective help text constants depending on which item has focus
	scannedItemsTable.SetFocusFunc(func() {
		footer.SetText(scannedItemsHelpText)
	})
	tableList.SetFocusFunc(func() {
		footer.SetText(tableListHelpText)
	})

	flex := tview.NewFlex().
		AddItem(tableList, 0, 1, true).
		AddItem(scannedItemsTable, 0, 3, false)

	grid := tview.NewGrid().
		SetRows(-20, 1).  // Scale footer 1/20 of table area which should be sufficient for fullscreen
		SetMinSize(2, 1). // Set minimum size so footer always displays on screens >= ~103x28
		SetBorders(true).
		AddItem(flex, 0, 0, 2, 2, 0, 0, true).
		AddItem(footer, 2, 0, 1, 2, 0, 0, false)

	return &TablePage{
		Grid:            grid,
		DynamoTableList: tableList,
		ScannedItems:    scannedItemsTable,
		HelpFooter:      footer,
		PreviewRowCount: rowCount,
		trimTableRows:   !disableTableTrim,
		trimTableWidth:  defaultTableTrimWidth,
	}
}

func (_ TablePage) GetTitle() string {
	return "TablePage"
}

// CurrentTableSelectionName returns the currently-selected table name
func (t TablePage) CurrentTableSelectionName() string {
	tableName, _ := t.DynamoTableList.GetItemText(t.DynamoTableList.GetCurrentItem())
	return tableName
}

// PopulateTableList populates the left-hand column with DynamoDB table names
func (t *TablePage) PopulateTableList(tableNames []string) {
	for _, tableName := range tableNames {
		t.DynamoTableList.AddItem(tableName, "", 0, nil)
	}
}

// PopulateTableItems takes a list of DynamoDB table items and writes them to the ScannedItems table
func (t *TablePage) PopulateTableItems(items []tables.Item, table *tables.Table) {
	// We'll first gather an ordered list of all attribute names across the items
	sortedAttributes := table.SortedAttributeNames(items)

	// Clear the table and write the column header
	t.ScannedItems.Clear()
	color := tcell.ColorDarkCyan
	for i, column := range sortedAttributes {
		t.ScannedItems.SetCell(
			0,
			i,
			tview.NewTableCell(column).SetTextColor(color).SetAlign(tview.AlignCenter))
	}

	// Iterate the items and populate the rows.
	// If an attribute is not present on an item a 'null' will be written.
	color = tcell.ColorWhite
	var strVal string
	for k, item := range items {
		var column int
		rowNum := k + 1

		for _, attribute := range sortedAttributes {
			if val, ok := item[attribute]; ok {
				if v, isBytes := val.([]byte); isBytes {
					strVal = string(v)
				} else {
					strVal = fmt.Sprintf("%v", val)
				}

				// If trim is set to true, and the length exceed max width, trim it
				if t.trimTableRows && len(strVal) > t.trimTableWidth {
					strVal = strVal[:t.trimTableWidth]
				}
			} else {
				strVal = "null"
			}

			t.ScannedItems.SetCell(
				rowNum,
				column,
				tview.NewTableCell(strVal).SetTextColor(color).SetAlign(tview.AlignCenter))
			column++
		}
	}
}
