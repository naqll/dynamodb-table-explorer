package pages

import (
	"encoding/json"
	"fmt"
	"github.com/rivo/tview"
	"os/exec"
	"runtime"
)

type JSONPage struct {
	*tview.Grid

	JSONText      *tview.TextView
	Footer        *tview.TextView
	ColorsEnabled bool // Whether or not tcell dynamic coloring is enabled
}

const helpText = `[green]ESC: [white]Close      [green]C: [white]Copy contents to clipboard`

// NewJSONPage returns an JSONPage used to display formatted JSON output
func NewJSONPage(disableJSONColors, disableJSONBorder bool) *JSONPage {
	// Setting dynamic colors allows parsing the '[color]' colors in the constants
	footer := tview.NewTextView().SetDynamicColors(true).
		SetText(helpText).
		SetTextAlign(tview.AlignCenter)
	jsonText := tview.NewTextView()
	jsonText.SetDynamicColors(!disableJSONColors)

	grid := tview.NewGrid().
		SetRows(-20, 1).  // Scale footer 1/20 of table area which should be sufficient for fullscreen
		SetMinSize(2, 1). // Set minimum size so footer always displays on screens >= ~103x28
		AddItem(jsonText, 0, 0, 2, 2, 0, 0, true).
		AddItem(footer, 2, 0, 1, 2, 0, 0, false)

	// Don't set the border if -disable-json-border CLI arg was passed
	grid.SetBorder(!disableJSONBorder)

	return &JSONPage{
		Grid:          grid,
		JSONText:      jsonText,
		Footer:        footer,
		ColorsEnabled: !disableJSONColors,
	}
}

func (_ JSONPage) GetTitle() string {
	return "JSONPage"
}

// PrettyPrintJSON marshals the JSON data with four-space indentation and writes it to the TextView.
// tview dynamic colors are respected.
func (j *JSONPage) PrettyPrintJSON(data interface{}) error {
	b, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return fmt.Errorf("error marshalling data: %s", err.Error())
	}
	j.JSONText.SetText(string(b))
	j.JSONText.ScrollToBeginning()
	return nil
}

// CopyToClipboard will copy the formatted contents of JSONPage into the clipboard.
// This only supports Linux and Darwin runtimes and only if xclip is available on Linux and pbcopy is available on OSX.
func (j JSONPage) CopyToClipboard() error {
	var clipCmd *exec.Cmd
	switch runtime.GOOS {
	case "darwin":
		clipCmd = exec.Command("pbcopy")
	case "linux":
		clipCmd = exec.Command("xclip", "-selection", "clipboard")
	default:
		return nil
	}

	if stdin, err := clipCmd.StdinPipe(); err != nil {
		return fmt.Errorf("error opening stdin for clipboard data: %s", err.Error())
	} else if err = clipCmd.Start(); err != nil {
		return fmt.Errorf("error running clipboard command: %s", err.Error())
	} else if _, err = stdin.Write([]byte(j.JSONText.GetText(true))); err != nil {
		return fmt.Errorf("error writing JSON text to stdin: %s", err.Error())
	} else if err = stdin.Close(); err != nil {
		return fmt.Errorf("error closing stdin: %s", err.Error())
	}

	return clipCmd.Wait()
}
