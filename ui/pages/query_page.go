package pages

import (
	"fmt"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"gitlab.com/naqll/dynamodb-table-explorer/pkg/tables"
)

type QueryPage struct {
	*tview.Grid

	Form         *tview.Form
	ResultsTable *tview.Table

	// Very large table items cause tcell rendering to lag.
	// This is exacerbated when large queries are run.
	// If trimTableRows is true, table item attributes will be trimmed to 60 characters.
	// The full item is still viewable in the JSON view.
	trimTableRows  bool
	trimTableWidth int
}

// NewQueryPage returns a Grid holding the query input form, and the table below the form for displaying query results
func NewQueryPage(disableTableTrim bool) *QueryPage {
	table := tview.NewTable()
	table.SetSelectable(true, false).
		SetBorders(true).
		SetFixed(1, 1).
		SetTitle("Query Results")

	form := tview.NewForm()
	grid := tview.NewGrid().
		SetRows(-1, -2).
		SetBorders(true).
		AddItem(form, 0, 0, 1, 1, 0, 0, true).
		AddItem(table, 1, 0, 1, 1, 0, 0, false)

	return &QueryPage{
		Grid:           grid,
		Form:           form,
		ResultsTable:   table,
		trimTableRows:  !disableTableTrim,
		trimTableWidth: defaultTableTrimWidth,
	}
}

func (_ QueryPage) GetTitle() string {
	return "QueryPage"
}

// GetInputFieldByLabel returns the input field with the given label
func (q QueryPage) GetInputFieldByLabel(label string) *tview.InputField {
	return q.Form.GetFormItemByLabel(label).(*tview.InputField)
}

// GetInputFieldText returns the current text in the input field with the given label
func (q QueryPage) GetInputFieldText(label string) string {
	return q.Form.GetFormItemByLabel(label).(*tview.InputField).GetText()
}

// GetDropdownSelection returns the current selection of the dropdown with the given label
func (q QueryPage) GetDropdownSelection(label string) string {
	_, val := q.Form.GetFormItemByLabel(label).(*tview.DropDown).GetCurrentOption()
	return val
}

// GetButtonByLabel returns the button with the given label or nil
func (q QueryPage) GetButtonByLabel(label string) *tview.Button {
	return q.Form.GetButton(q.Form.GetButtonIndex(label))
}

// CurrentTableIndexName returns the index name for the currently-selected index
func (q QueryPage) CurrentTableIndexName(table *tables.Table) string {
	tableIndex := q.GetCurrentIndexSelection(table)
	return tableIndex.Name
}

// CurrentHashKeyName returns the hash key attribute name for the currently-selected index
func (q QueryPage) CurrentHashKeyName(table *tables.Table) string {
	tableIndex := q.GetCurrentIndexSelection(table)
	return *tableIndex.HashKey
}

// CurrentHashKeyFieldName returns the hash key attribute form-field name for the currently-selected index
func (q QueryPage) CurrentHashKeyFieldName(table *tables.Table) string {
	tableIndex := q.GetCurrentIndexSelection(table)
	return tableIndex.HashKeyString()
}

// CurrentRangeKeyName returns the range key attribute name for the currently-selected index or nil
func (q QueryPage) CurrentRangeKeyName(table *tables.Table) *string {
	tableIndex := q.GetCurrentIndexSelection(table)
	return tableIndex.RangeKey
}

// CurrentRangeKeyFieldName returns the range key attribute form-field name for the currently-selected index or nil
func (q QueryPage) CurrentRangeKeyFieldName(table *tables.Table) *string {
	tableIndex := q.GetCurrentIndexSelection(table)
	return tableIndex.RangeKeyString()
}

// GetCurrentIndexSelection returns the tables.Index selected in the "Index" dropdown field
func (q QueryPage) GetCurrentIndexSelection(table *tables.Table) *tables.Index {
	tableIndex := q.GetDropdownSelection("Index")
	return table.GetIndex(tableIndex)
}

// PopulateForm populates the query input form items using table information.
func (q *QueryPage) PopulateForm(table *tables.Table) {
	// Clear the form and create the inputs.
	q.Form.Clear(true)
	q.Form.AddDropDown("Index", table.AllIndexes(), 0, nil)
	q.Grid.SetTitle(*table.TableName)

	// Add field for hash key
	q.Form.AddInputField(table.HashKeyString(), "", 30, func(text string, _ rune) bool {
		return table.HashKeyType.Validate(text)
	}, nil)

	// Add field for optional range key
	if table.RangeKey != nil {
		q.Form.AddInputField(*table.RangeKeyString(), "", 30, func(text string, _ rune) bool {
			return table.RangeKeyType.Validate(text)
		}, nil)

		// Add condition dropdown if range key is present
		q.Form.AddDropDown("Range condition", tables.KeyConditions, 0, nil)
	}
	// Do not set the dropdown callback func until _after_ the above input fields are set
	dropDown := q.Form.GetFormItemByLabel("Index").(*tview.DropDown)
	dropDown.SetSelectedFunc(func(indexName string, _ int) {
		index := table.GetIndex(indexName)
		q.updateInputFieldsByIndexName(index)
	})

	// Add button and skip setting the callback func as it's defined as an input capture
	q.Form.AddButton("Go!", nil)
	q.Form.SetButtonBackgroundColor(tcell.ColorDarkCyan)
	q.Form.SetFieldBackgroundColor(tcell.ColorDarkCyan)
}

// PopulateTableResults populates the table with the queried DynamoDB items stored in table.QueryResults
func (q *QueryPage) PopulateTableResults(table *tables.Table) {
	// We'll first gather an ordered list of all attribute names across the items
	sortedAttributes := table.SortedAttributeNames(table.QueryResults)

	// Clear the table and write the column header
	q.ResultsTable.Clear()
	q.PopulateColumnHeader(sortedAttributes)

	// Iterate the items and populate the rows.
	// If an attribute is not present on an item a 'null' will be written.
	color := tcell.ColorWhite
	var strVal string
	for k, item := range table.QueryResults {
		var column int
		rowNum := k + 1

		for _, attribute := range sortedAttributes {
			if val, ok := item[attribute]; ok {
				if v, isBytes := val.([]byte); isBytes {
					strVal = string(v)
				} else {
					strVal = fmt.Sprintf("%v", val)
				}

				// If trim is set to true, and the length exceed max width, trim it
				if q.trimTableRows && len(strVal) > q.trimTableWidth {
					strVal = strVal[:q.trimTableWidth]
				}

			} else {
				strVal = tables.NullValue
			}

			q.ResultsTable.SetCell(
				rowNum,
				column,
				tview.NewTableCell(strVal).SetTextColor(color).SetAlign(tview.AlignCenter))
			column++
		}
	}
}

// PopulateColumnHeader clears the table and populates the column header in the table
func (q *QueryPage) PopulateColumnHeader(columns []string) {
	q.ResultsTable.Clear()
	color := tcell.ColorDarkCyan
	for i, col := range columns {
		q.ResultsTable.SetCell(0, i, tview.NewTableCell(col).SetTextColor(color).SetAlign(tview.AlignCenter))
	}
}

// Query performs a DynamoDB query; this is the action of the 'Go!' button in the QueryPage form
func (q *QueryPage) Query(table *tables.Table) error {
	// Get the currently-selected index name in the dropdown field, and
	// hash key/range key form-field names from the input fields
	indexName := q.CurrentTableIndexName(table)
	hashKeyFieldName := q.CurrentHashKeyFieldName(table)
	rangeKeyFieldName := q.CurrentRangeKeyFieldName(table)

	// Get the hash key user input
	hashValue := q.GetInputFieldText(hashKeyFieldName)

	// Get the optional range key user input and conditional dropdown value
	var rangeValue *string
	var conditionValue string
	if rangeKeyFieldName != nil {
		rk := q.GetInputFieldText(*rangeKeyFieldName)
		if rk != "" {
			rangeValue = &rk
		}
		conditionValue = q.GetDropdownSelection("Range condition")
	}

	index := table.GetIndex(indexName)
	expr, err := index.ConstructExpression(hashValue, rangeValue, conditionValue)
	if err != nil {
		return fmt.Errorf("error constructing query expression: %s", err.Error())
	}

	input := &dynamodb.QueryInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 table.TableDescription.TableName,
	}

	// Set the index name if non-primary
	if indexName != tables.PrimaryIndex {
		input.IndexName = &indexName
	}

	tableItems, err := table.Query(input)
	if err != nil {
		return fmt.Errorf("error querying table: %s", err.Error())
	}
	table.QueryResults = tableItems
	q.PopulateTableResults(table)
	return nil
}

// updateInputFieldsByIndexName updates the input field labels with the appropriate index attribute key names
// and acceptance funcs.
// This is a callback func triggered when a table index is selected in the form dropdown menu.
func (q *QueryPage) updateInputFieldsByIndexName(index *tables.Index) {
	// This is cumbersome as the field names are not static so we must use indices here
	hashKeyField := q.Form.GetFormItem(1).(*tview.InputField)
	var rangeKeyField *tview.InputField
	if q.Form.GetFormItemCount() > 2 {
		rangeKeyField = q.Form.GetFormItem(2).(*tview.InputField)
	}

	hashKeyField.SetLabel(index.HashKeyString())
	hashKeyField.SetAcceptanceFunc(func(text string, _ rune) bool {
		return index.HashKeyType.Validate(text)
	})

	if index.RangeKey != nil {
		if rangeKeyField == nil {
			// If the newly selected index has a range key, but the old index did not, we'll create an input field
			q.Form.AddInputField(*index.RangeKeyString(), "", 30, func(text string, _ rune) bool {
				return index.RangeKeyType.Validate(text)
			}, nil)
		} else {
			// If the newly selected index has a range key, and the old index did, we'll simply update the label and func.
			// Do not remove the acceptance func as the new key could be of a different type.
			rangeKeyField.SetLabel(*index.RangeKeyString())
			rangeKeyField.SetAcceptanceFunc(func(text string, _ rune) bool {
				return index.RangeKeyType.Validate(text)
			})
		}

		if i := q.Form.GetFormItemIndex("Range condition"); i == -1 {
			// Add condition dropdown if range key is present
			q.Form.AddDropDown("Range condition", tables.KeyConditions, 0, nil)
		}
		// Add condition dropdown if range key is present
		//q.Form.AddDropDown("Range condition", tables.FilterConditions, 0, nil)

	} else if rangeKeyField != nil {
		// The current index selection only has a hash key so we'll delete the extra fields.
		// Once for range key and again for condition dropdown.
		q.Form.RemoveFormItem(2)
		q.Form.RemoveFormItem(2)
	}
}
