package key_type

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// KeyType represents one of the DynamoDB attribute value types
type KeyType interface {
	AttributeValue(searchKey string) *dynamodb.AttributeValue
	Validate(keyValue string) bool
	ConvertToType(keyValue interface{}) interface{}
}

type StringKeyType struct{}
type BinaryKeyType struct{}
type NumberKeyType struct{}

// NewKeyType returns a new KeyType for the provided DynamoDB attribute type
func NewKeyType(s string) KeyType {
	switch strings.ToLower(s) {
	case "s":
		return &StringKeyType{}
	case "b":
		return &BinaryKeyType{}
	case "n":
		return &NumberKeyType{}
	default:
		return nil
	}
}

func (_ StringKeyType) String() string {
	return "S"
}

func (_ BinaryKeyType) String() string {
	return "B"
}

func (_ NumberKeyType) String() string {
	return "N"
}

// ConvertToType converts the keyValue to a string
func (s StringKeyType) ConvertToType(keyValue interface{}) interface{} {
	switch t := keyValue.(type) {
	case string:
		return t
	case int:
		return strconv.Itoa(t)
	default:
		return fmt.Sprintf("%v", t)
	}
}

// ConvertToType converts the keyValue to []byte.
// The value is automatically base64 encoded/decoded by the DynamoDB SDK.
func (b BinaryKeyType) ConvertToType(keyValue interface{}) interface{} {
	switch t := keyValue.(type) {
	case string:
		return []byte(t)
	case int:
		return []byte(strconv.Itoa(t))
	default:
		return []byte(fmt.Sprintf("%v", t))
	}
}

// ConvertToType converts the keyValue to an integer
func (n NumberKeyType) ConvertToType(keyValue interface{}) interface{} {
	switch t := keyValue.(type) {
	case string:
		if i, err := strconv.Atoi(t); err != nil {
			return fmt.Sprintf("%v", t)
		} else {
			return i
		}
	case int:
		return t
	default:
		return fmt.Sprintf("%v", t)
	}
}

// AttributeValue returns a string attribute
func (_ StringKeyType) AttributeValue(searchKey string) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{
		S: &searchKey,
	}
}

// AttributeValue returns a []byte/binary attribute
func (_ BinaryKeyType) AttributeValue(searchKey string) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{
		B: []byte(searchKey),
	}
}

// AttributeValue returns a number attribute
func (_ NumberKeyType) AttributeValue(searchKey string) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{
		N: &searchKey,
	}
}

// Validate returns true if the value is a string. This is always true.
func (s StringKeyType) Validate(_ string) bool { return true }

// Validate returns true if the value is a string. This is always true.
func (b BinaryKeyType) Validate(_ string) bool { return true }

// Validate returns true if the value is an integer
func (n NumberKeyType) Validate(keyValue string) bool {
	_, err := strconv.Atoi(keyValue)
	return err == nil
}
