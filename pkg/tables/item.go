package tables

import "fmt"

const (
	colorWhite  = "white"
	colorGreen  = "green"
	colorBlue   = "blue"
	colorRed    = "red"
	colorYellow = "yellow"
)

// Item represents a DynamoDB table item
type Item map[string]interface{}

// ColoredCopy returns a coyp of Item with all keys colored blue and all string values colored green.
// Note: If we color non-string values they will be marshalled as strings.
func (i Item) ColoredCopy() Item {
	coloredItem := Item{}
	for k, v := range i {
		key := colorString(colorBlue, k)
		value := parseAndColorStringValues(v)
		coloredItem[key] = value
	}
	return coloredItem
}

// colorMap recursively adds colors to map keys/values
func colorMap(m map[string]interface{}) map[string]interface{} {
	newMap := make(map[string]interface{})
	for k, v := range m {
		key := colorString(colorBlue, k)
		value := parseAndColorStringValues(v)
		newMap[key] = value
	}
	return newMap
}

// colorSlice recursively adds colors to slice values
func colorSlice(sl []interface{}) []interface{} {
	var newSlice []interface{}
	for _, v := range sl {
		newSlice = append(newSlice, parseAndColorStringValues(v))
	}
	return newSlice
}

// parseAndColorStringValues colors a string values green.
// Maps and slices will recursively call this function through their handlers.
func parseAndColorStringValues(v interface{}) interface{} {
	var value interface{}
	switch v.(type) {
	case string:
		value = colorString(colorGreen, v)
	case []byte:
		value = colorString(colorGreen, string(v.([]byte)))
	case int:
		value = v
	case []interface{}:
		value = colorSlice(v.([]interface{}))
	case map[string]interface{}:
		value = colorMap(v.(map[string]interface{}))
	default:
		value = v
	}
	return value
}

// colorString returns a string with the tcell dynamic color name set.
// The white color will always follow the provided value "resetting" the coloring.
func colorString(colorName string, val interface{}) string {
	return fmt.Sprintf("[%s]%v[%s]", colorName, val, colorWhite)
}
