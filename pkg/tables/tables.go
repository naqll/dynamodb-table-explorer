package tables

import (
	"fmt"
	"sort"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"

	key "gitlab.com/naqll/dynamodb-table-explorer/pkg/key_type"
)

// Table holds a DynamoDB table description along with some custom fields derived from the description
type Table struct {
	*dynamodb.TableDescription
	client dynamodbiface.DynamoDBAPI

	*Index                    // The default table PrimaryIndex
	SecondaryIndexes []*Index // Additional indexes, GSI's etc
	SortedKeyNames   []string // An ordered list of hash key, range key, followed by alphabetically sorted GSI keys

	QueryResults []Item // The most recent DynamoDB table query results
	ScanResults  []Item // The most recent DynamoDB table scan results
}

const NullValue = "null"

// NewTableDetails returns a TableDetails item with the embedded description and DynamoDB client.
// This populates all fields and secondary indexes on the TableDetails object.
func NewTableDetails(description *dynamodb.TableDescription, dynamoClient dynamodbiface.DynamoDBAPI) *Table {
	t := &Table{
		TableDescription: description,
		client:           dynamoClient,
		Index: &Index{
			Name: PrimaryIndex,
		},
	}

	// Set the hash and optional range key attribute names
	for _, schema := range t.KeySchema {
		if *schema.KeyType == dynamodb.KeyTypeHash {
			t.HashKey = schema.AttributeName
		} else if *schema.KeyType == dynamodb.KeyTypeRange {
			t.RangeKey = schema.AttributeName
		}
	}

	// Set the hash and optional range key attribute types
	for _, attr := range t.AttributeDefinitions {
		if *attr.AttributeName == *t.HashKey {
			t.HashKeyType = key.NewKeyType(*attr.AttributeType)
		} else if t.RangeKey != nil && *attr.AttributeName == *t.RangeKey {
			t.RangeKeyType = key.NewKeyType(*attr.AttributeType)
		}
	}

	// Populate the secondary index list
	t.setSecondaryIndexes()
	// Sort the key names
	t.setSortedKeyNames()

	return t
}

// Query performs a DynamoDB Query operation with the provided input.
// Results are unmarshalled into a slice of Item.
func (t Table) Query(input *dynamodb.QueryInput) ([]Item, error) {
	resp, err := t.client.Query(input)
	if err != nil {
		return nil, fmt.Errorf("error querying DynamoDB table: %s", err.Error())
	}

	var results []Item
	if err = dynamodbattribute.UnmarshalListOfMaps(resp.Items, &results); err != nil {
		return nil, fmt.Errorf("error unmarshalling table items: %s", err.Error())
	}
	return results, nil
}

// GetItem performs a DynamoDB GetItem operation with the provided input.
// Results are unmarshalled into an Item.
func (t Table) GetItem(input *dynamodb.GetItemInput) (Item, error) {
	var item Item
	if resp, err := t.client.GetItem(input); err != nil {
		return nil, fmt.Errorf("error getting item from DynamoDB table: %s", err.Error())
	} else if err = dynamodbattribute.UnmarshalMap(resp.Item, &item); err != nil {
		return nil, fmt.Errorf("error unmarshalling item from DynamoDB: %s", err.Error())
	}
	return item, nil
}

// Scan performs a DynamoDB Scan operation with the provided input.
// Results are unmarshalled into a slice of Item.
func (t Table) Scan(input *dynamodb.ScanInput) ([]Item, error) {
	resp, err := t.client.Scan(input)
	if err != nil {
		return nil, fmt.Errorf("error scanning DynamoDB table: %s", err.Error())
	}

	var results []Item
	if err = dynamodbattribute.UnmarshalListOfMaps(resp.Items, &results); err != nil {
		return nil, fmt.Errorf("error unmarshalling table items: %s", err.Error())
	}
	return results, nil
}

// GetIndex returns the index (primary or secondary) with the given name, or nil
func (t Table) GetIndex(indexName string) *Index {
	if indexName == PrimaryIndex {
		return t.Index
	}

	for _, si := range t.SecondaryIndexes {
		if si.Name == indexName {
			return si
		}
	}
	return nil
}

// AllIndexes returns all table indexes used for QueryPage form dropdown
func (t Table) AllIndexes() []string {
	indexes := []string{PrimaryIndex}
	for _, si := range t.SecondaryIndexes {
		indexes = append(indexes, si.Name)
	}
	return indexes
}

// SortedAttributeNames returns an ordered list of all attributes across the items.
// The ordering is as follows:
//   - The table hash key
//   - The table range key (if present)
//   - Alphabetically sorted GSI index keys (if present; sorting is applied in constructor)
//   - Alphabetically sorted other keys
func (t Table) SortedAttributeNames(items []Item) []string {
	// Create map to track found attributes
	var allAttributes []string
	foundAttributes := make(map[string]struct{})

	for _, keyName := range t.SortedKeyNames {
		allAttributes = append(allAttributes, keyName)
		foundAttributes[keyName] = struct{}{}
	}

	// Add all unique attributes from the items
	var extraAttributes []string
	for _, item := range items {
		for k := range item {
			if _, ok := foundAttributes[k]; !ok {
				foundAttributes[k] = struct{}{}
				extraAttributes = append(extraAttributes, k)
			}
		}
	}

	// Sort the extra attributes and append to our list
	sort.Strings(extraAttributes)
	allAttributes = append(allAttributes, extraAttributes...)
	return allAttributes
}

// setSecondaryIndexes iterates the global secondary indexes from the table description adding both the
// attribute names and attribute types to t.SecondaryIndexes
func (t *Table) setSecondaryIndexes() {
	for _, gsi := range t.GlobalSecondaryIndexes {
		secondaryIndex := &Index{
			Name: *gsi.IndexName,
		}

		// Set the hash and range key names
		for _, element := range gsi.KeySchema {
			if *element.KeyType == dynamodb.KeyTypeHash {
				secondaryIndex.HashKey = element.AttributeName
			} else if *element.KeyType == dynamodb.KeyTypeRange {
				secondaryIndex.RangeKey = element.AttributeName
			}
		}

		// Set the hash and range key attribute types
		for _, attr := range t.AttributeDefinitions {
			if *attr.AttributeName == *secondaryIndex.HashKey {
				secondaryIndex.HashKeyType = key.NewKeyType(*attr.AttributeType)
			} else if secondaryIndex.RangeKey != nil && *attr.AttributeName == *secondaryIndex.RangeKey {
				secondaryIndex.RangeKeyType = key.NewKeyType(*attr.AttributeType)
			}
		}

		t.SecondaryIndexes = append(t.SecondaryIndexes, secondaryIndex)
	}
}

// setSortedKeyNames sets the SortedKeyNames list on the TableDetails.
// The ordering is as follows:
//   - The table hash key
//   - The table range key (if present)
//   - Alphabetically sorted GSI index keys (if present)
func (t *Table) setSortedKeyNames() {
	// Create map to track found attributes
	foundAttributes := map[string]struct{}{
		*t.HashKey: {},
	}

	// Add the hash and optional range keys
	t.SortedKeyNames = append(t.SortedKeyNames, *t.HashKey)
	if t.RangeKey != nil {
		t.SortedKeyNames = append(t.SortedKeyNames, *t.RangeKey)
		foundAttributes[*t.RangeKey] = struct{}{}
	}

	// Add the secondary index keys which could overlap/be duplicates
	var secondary []string
	for _, index := range t.SecondaryIndexes {
		if _, ok := foundAttributes[*index.HashKey]; !ok {
			secondary = append(secondary, *index.HashKey)
			foundAttributes[*index.HashKey] = struct{}{}
		}
		if index.RangeKey != nil {
			if _, ok := foundAttributes[*index.RangeKey]; !ok {
				secondary = append(secondary, *index.RangeKey)
				foundAttributes[*index.RangeKey] = struct{}{}
			}
		}
	}

	// Sort the secondary indexes and add to our attributes list
	sort.Strings(secondary)
	t.SortedKeyNames = append(t.SortedKeyNames, secondary...)
}
