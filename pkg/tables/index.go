package tables

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"

	key "gitlab.com/naqll/dynamodb-table-explorer/pkg/key_type"
)

// Index is a table index, primary or GSI
type Index struct {
	Name         string
	HashKey      *string
	RangeKey     *string
	HashKeyType  key.KeyType
	RangeKeyType key.KeyType
}

const (
	PrimaryIndex = "Primary"

	Equal              = "="
	LessThan           = "<"
	LessThanOrEqual    = "<="
	GreaterThan        = ">"
	GreaterThanOrEqual = ">="
	// NotEqual          = "!="      // These conditionals are not yet implemented in ConstructQuery()
	//Between            = "BETWEEN"
	//Exists             = "NOT_NULL"
	//NotExists          = "NULL"
	//Contains           = "CONTAINS"
	//NotContains        = "NOT_CONTAINS"
	//BeginsWith         = "BEGINS_WITH"
)

var KeyConditionsMap = map[string]func(expression.KeyBuilder, expression.ValueBuilder) expression.KeyConditionBuilder{
	Equal:              expression.KeyEqual,
	LessThan:           expression.KeyLessThan,
	LessThanOrEqual:    expression.KeyLessThanEqual,
	GreaterThan:        expression.KeyGreaterThan,
	GreaterThanOrEqual: expression.KeyGreaterThanEqual,
}

// KeyConditions is a list of ordered keys from KeyConditionsMap
// TODO we need a better solution for this as the map keys are unordered but '=' should probably always appear first in the dropdown list
var KeyConditions = []string{
	Equal,
	LessThan,
	LessThanOrEqual,
	GreaterThan,
	GreaterThanOrEqual,
}

// HashKeyString returns a string with the hash key value and its single letter type (S|B|N)
func (i Index) HashKeyString() string {
	return fmt.Sprintf("%s (%s)", *i.HashKey, i.HashKeyType)
}

// RangeKeyString returns a string with the hash key value and its single letter type (S|B|N).
func (i Index) RangeKeyString() *string {
	if i.RangeKey == nil {
		return nil
	}
	return aws.String(fmt.Sprintf("%s (%s)", *i.RangeKey, i.RangeKeyType))
}

// ConstructExpression constructs an expression with an optional range key condition
func (i Index) ConstructExpression(hashKeyValue string, rangeKeyValue *string, keyFilterCondition string) (expression.Expression, error) {
	keyCondition := i.ConstructKeyCondition(hashKeyValue, rangeKeyValue, keyFilterCondition)
	return expression.NewBuilder().WithKeyCondition(keyCondition).Build()
}

// ConstructKeyCondition constructs a KeyConditionBuilder using the index hash and optional range key names/values.
// This uses the condition operator selected in the dropdown menu of the QueryPage form.
func (i Index) ConstructKeyCondition(hashKeyValue string, rangeKeyValue *string, keyFilterCondition string) expression.KeyConditionBuilder {
	// We will always have a hash key condition
	keyCondition := expression.Key(*i.HashKey).Equal(expression.Value(i.HashKeyType.ConvertToType(hashKeyValue)))

	// We may also receive an optional range key condition
	if rangeKeyFilterFunc, ok := KeyConditionsMap[keyFilterCondition]; ok && rangeKeyValue != nil {
		rangeCondition := rangeKeyFilterFunc(
			expression.Key(*i.RangeKey),
			expression.Value(i.RangeKeyType.ConvertToType(*rangeKeyValue)))

		return expression.KeyAnd(keyCondition, rangeCondition)
	}

	return keyCondition
}
