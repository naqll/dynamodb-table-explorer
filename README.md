## DynamoDB Table Explorer
Read-only TUI application for exploring DynamoDB tables. 

![DDB Table Page](./readme_src/demo.gif)


It supports:
  - Preview scans of tables
  - Querying tables via hash and optional range key
  - Viewing table items/query results as formatted/copyable JSON
  - Viewing some table metadata:
    - Hash key
    - Range key
    - Table ARN
    - Table status
    - Table size
    - Item count
    - Global Secondary Indexes

### Usage
Launching the program optionless will use the default system credentials/region in standard AWS order of precedence.  
Optional `-r [REGION]` and `-p [PROFILE]` arguments can be passed to specify the region and AWS profile respectively. 

Optional flags:
  - `-row-count` (default 20) Defines the number of rows to scan when previewing a table
  - `-disable-trim` (default false) Disable trimming of large table fields/attributes to 60 characters in table views
    - The full object is always available in JSON views regardless of this value
  - `-disable-json-colors` (default false) Disable dynamic coloring in JSON pretty-print views
  - `-disable-json-border` (default false) Disables the page border when viewing formatted JSON
  - `-p` (default system credentials) AWS profile name to use for session
  - `-r` (default system region) AWS region to use for session


`ddb [-r REGION] [-p AWS_PROFILE] [-row-count ROW_COUNT] [-disable-trim] [-disable-json-colors]`


### TODO
  - Add pagination support for scans and queries
