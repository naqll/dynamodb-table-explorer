package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

var (
	region            = flag.String("r", "", "AWS region -- if none, the default system region will be used")
	profile           = flag.String("p", "", "AWS profile name -- if none, the default system credentials will be used")
	rowCount          = flag.Int("row-count", 20, "Number of rows to load when previewing a table")
	disableTrim       = flag.Bool("disable-trim", false, "Disable trimming of table attribute fields to 60 characters while in table view (extremely large table items will cause lag; the full item is always available in JSON view)")
	disableJSONColors = flag.Bool("disable-json-colors", false, "Disable dynamic coloring in JSON pretty-print pages")
	disableJSONBorder = flag.Bool("disable-json-border", false, "Disable the border on JSON pretty-print pages")

	explorer         *Explorer
	dynamoClient     dynamodbiface.DynamoDBAPI
	dynamoTableNames []string
)

func main() {
	explorer = NewExplorer(*disableTrim, *disableJSONColors, *disableJSONBorder, *rowCount)
	// Keep mouse disabled as it breaks JSON page selections
	if err := explorer.SetRoot(explorer.Pages, true).EnableMouse(false).Run(); err != nil {
		log.Fatalf("error running app: %s", err.Error())
	}
}

// populateDynamoDBTables populates the global dynamoTableNames list
func populateDynamoDBTables() error {
	resp, err := dynamoClient.ListTables(nil)
	if err != nil {
		return fmt.Errorf("error listing DynamoDB tables: %s", err.Error())
	}
	for _, tableName := range resp.TableNames {
		dynamoTableNames = append(dynamoTableNames, *tableName)
	}
	return nil
}

func init() {
	flag.Parse()

	opts := session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}
	if *region != "" {
		opts.Config.Region = region
	}
	if *profile != "" {
		opts.Profile = *profile
	}

	sess := session.Must(session.NewSessionWithOptions(opts))
	dynamoClient = dynamodb.New(sess)
	if err := populateDynamoDBTables(); err != nil {
		log.Fatalf("error listing DynamoDB tables: %s", err.Error())
	}
}
