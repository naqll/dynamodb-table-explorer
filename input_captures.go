package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

// setTablePageInputCaptures defines the inputs that we want to handle within this sub-resource
func (ex *Explorer) setTablePageInputCaptures() {

	tableList := ex.TablePage.DynamoTableList
	scannedItems := ex.TablePage.ScannedItems

	ex.TablePage.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {

		case tcell.KeyTab:
			if ex.TablePage.DynamoTableList.HasFocus() && ex.TablePage.CurrentTableSelection != nil {
				ex.SetFocus(ex.TablePage.ScannedItems)
			} else if ex.TablePage.ScannedItems.HasFocus() {
				ex.SetFocus(ex.TablePage.DynamoTableList)
			}
			return nil

		case tcell.KeyEnter:
			// If a table is selected, we'll scan the first X (-c flag) items and populate the table rows.
			// If a table item is selected, we'll display the item in the JSON page as copyable text.
			if tableList.HasFocus() && len(dynamoTableNames) > 0 {
				// Move highlighted row back to column header
				ex.TablePage.ScannedItems.Select(0, 0)

				// Get the current table and scan the first X (-c flag) items
				table, err := ex.SetCurrentTableSelection()
				if err != nil {
					ex.DisplayError(fmt.Errorf("error getting table details: %s", err.Error()))
					return nil
				}

				input := &dynamodb.ScanInput{
					TableName: table.TableName,
					Limit:     aws.Int64(int64(ex.TablePage.PreviewRowCount)),
				}

				if table.ScanResults, err = table.Scan(input); err != nil {
					ex.DisplayError(fmt.Errorf("error scanning table: %s", err.Error()))
					return nil
				}
				ex.TablePage.PopulateTableItems(table.ScanResults, table)
				ex.TablePage.ScannedItems.ScrollToBeginning()

			} else if scannedItems.HasFocus() {
				// If enter is pressed while a valid table row is selected, we'll send the row
				// data to the JSON page for display
				table := ex.TablePage.CurrentTableSelection
				if rowNum, _ := ex.TablePage.ScannedItems.GetSelection(); rowNum <= 0 {
					return nil
				} else if err := ex.DisplayJSONItem(table.ScanResults[rowNum-1]); err != nil {
					ex.DisplayError(fmt.Errorf("error displaying scan result item: %s", err.Error()))
				}
			}

		case tcell.KeyF2:
			// Show table metadata/info -- same as 'D' key
			ex.showTableInfoPage()
		case tcell.KeyF3:
			// Create QueryPage form and switch to QueryPage -- Same as 'Q'
			ex.switchToQueryPage()
		case tcell.KeyRune:
			if event.Rune() == 'd' {
				// Show table metadata/info -- same as F2 key
				ex.showTableInfoPage()
			} else if event.Rune() == 'q' {
				// Create QueryPage form and switch to QueryPage -- Same as F3
				ex.switchToQueryPage()
			}
		}

		return event
	})
}

// setQueryPageInputCaptures defines the inputs that we want to handle within this sub-resource
func (ex *Explorer) setQueryPageInputCaptures() {
	ex.QueryPage.Grid.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {

		case tcell.KeyEnter:
			if ex.QueryPage.ResultsTable.HasFocus() {
				// If enter is pressed while a valid table row is selected, we'll send the row
				// data to the JSON page for display
				table := ex.TablePage.CurrentTableSelection
				if rowNum, _ := ex.QueryPage.ResultsTable.GetSelection(); rowNum <= 0 {
					return nil
				} else if err := ex.DisplayJSONItem(table.QueryResults[rowNum-1]); err != nil {
					ex.DisplayError(fmt.Errorf("error displaying table item: %s", err.Error()))
				}
			} else if _, btnIdx := ex.QueryPage.Form.GetFocusedItemIndex(); btnIdx == ex.QueryPage.Form.GetButtonIndex("Go!") {
				// This means ENTER was pressed on the 'Go!' button.
				//
				// We define the action for this event here instead of as a button callback function as the button
				// callback function does not have a good way to call our ErrorPage without passing tview.Pages around.
				if err := ex.QueryPage.Query(ex.TablePage.CurrentTableSelection); err != nil {
					ex.DisplayError(err)
				}
				return nil
			}
			return event

		case tcell.KeyTab:
			if ex.QueryPage.Form.HasFocus() {
				// This will work as long as we have only a single button
				if _, buttonIndex := ex.QueryPage.Form.GetFocusedItemIndex(); buttonIndex > -1 {
					// If the 'Go!' button is selected when tab is pushed, move focus to table
					ex.SetFocus(ex.QueryPage.ResultsTable)
					return nil
				}
			} else if ex.QueryPage.ResultsTable.HasFocus() {
				ex.QueryPage.Form.SetFocus(0)
				ex.SetFocus(ex.QueryPage.Form)
				return nil
			}
			return event

		case tcell.KeyRune:
			// If one of the form input fields is selected, we noop on the VIM keybindings
			if _, ok := ex.GetFocus().(*tview.InputField); ok {
				return event
			}

			// If a form input field is _not_ selected we'll map VIM keys to movement as we do in the main thread
			switch event.Rune() {
			case 'j':
				return tcell.NewEventKey(tcell.KeyDown, 'j', tcell.ModNone)
			case 'k':
				return tcell.NewEventKey(tcell.KeyUp, 'k', tcell.ModNone)
			case 'h':
				return tcell.NewEventKey(tcell.KeyLeft, 'h', tcell.ModNone)
			case 'l':
				return tcell.NewEventKey(tcell.KeyRight, 'l', tcell.ModNone)
			}
			return nil

		case tcell.KeyEscape:
			ex.Pages.SwitchToPage(ex.TablePage.GetTitle())
			return nil
		}
		return event
	})
}

// setTableInfoPageInputCaptures defines the inputs that we want to handle within this sub-resource
func (ex *Explorer) setTableInfoPageInputCaptures() {
	ex.TableInfoPage.Grid.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEscape:
			ex.Pages.HidePage(ex.TableInfoPage.GetTitle())
			return nil
		}
		return event
	})
}

// setJSONPageInputCaptures defines the inputs that we want to handle within this sub-resource
func (ex *Explorer) setJSONPageInputCaptures() {
	ex.JSONPage.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyRune:
			if event.Rune() == 'c' {
				if err := ex.JSONPage.CopyToClipboard(); err != nil {
					ex.DisplayError(fmt.Errorf("error copying data to clipboard: %s", err.Error()))
				}
				return nil
			}
		case tcell.KeyEscape:
			ex.Pages.HidePage(ex.JSONPage.GetTitle())

			if ex.TablePage.HasFocus() {
				ex.SetFocus(ex.TablePage.ScannedItems)
			} else if ex.QueryPage.HasFocus() {
				ex.SetFocus(ex.QueryPage.ResultsTable)
			}
			return nil
		}
		return event
	})
}

// setErrorPageInputCapture defines the inputs that we want to handle within this sub-resource
func (ex *Explorer) setErrorPageInputCapture() {
	ex.ErrorPage.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEscape:
			ex.Pages.HidePage(ex.ErrorPage.GetTitle())
			return nil
		}
		return event
	})
}
